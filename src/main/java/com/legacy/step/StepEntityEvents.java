package com.legacy.step;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class StepEntityEvents
{
	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event)
	{
		LivingEntity entity = event.getEntityLiving();

		if (entity instanceof PlayerEntity)
		{
			float enchantmentLevel = EnchantmentHelper.getMaxEnchantmentLevel(StepRegistry.STEPPING, entity);
			float defaultStepHeight = 0.6F;

			if (enchantmentLevel > 0 && entity.stepHeight < enchantmentLevel)
			{
				entity.stepHeight = enchantmentLevel + 0.1F;
			}
			else if (entity.stepHeight > defaultStepHeight)
			{
				entity.stepHeight = defaultStepHeight;
			}
		}
	}
}
